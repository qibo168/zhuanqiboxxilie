<?php
 

namespace app\dedetox1\index;
use app\common\controller\IndexBase;
use think\Db;
class Index extends IndexBase{
  
	/**
	 * 转换首页 开始清理表
	 */
	public function index(){
		$prefix = config('database.prefix');
		Db::execute("TRUNCATE ".$prefix."cms_category");
		Db::execute("TRUNCATE ".$prefix."cms_content");
		Db::execute("TRUNCATE ".$prefix."cms_content1");
		Db::execute("TRUNCATE ".$prefix."cms_content2");
		Db::execute("TRUNCATE ".$prefix."cms_content3");
		Db::execute("TRUNCATE ".$prefix."cms_info");
		Db::execute("TRUNCATE ".$prefix."cms_sort");
		 
		$this->success('数据表清空完毕 开始转换栏目数据',"dedetox1/index/lanmu");
	}

	 
 public function lanmu($page=1){
		$list=Db::connect('db_config1')->name('arctype')->where('channeltype',1)->limit(100)->page($page)->select();
		 foreach($list as $rs){
				$data=[
					'id'=>$rs['id'],
					'pid'=>$rs['topid'],
					'mid'=>1,
					'name'=>$rs['typename']
				];
				Db::name('cms_sort')->insert($data);
			}
if (!empty($list)){
	$page++;
	$pa=$page-1;
	$this->success("内容正在转换中，你可以去喝杯茶歇会... 第 {$pa} 页",url("dedetox1/index/lanmu",['page' =>$page]),'',1);
	exit;
}else{
	$this->success('栏目转换完毕 开始转换内容',"dedetox1/index/neirong");
}
		
	}
	 

 
	public function neirong($page=1){
		$list=Db::connect('db_config1')->name('archives')->alias('A')->join('addonarticle B','A.id = B.aid','LEFT')->field('A.*,B.body')->where('A.channel',1)->limit(100)->page($page)->select();
		 foreach($list as $rs){
				$rs['body']= str_replace('/uploads/',"/public/uploads/",$rs['body']);
			 $ispic=$rs['litpic']?1:0;
				$data=[
					'fid'=>$rs['typeid'],
					'mid'=>1,
					'title'=>$rs['title'],
					'ispic'=>$ispic,
					'uid'=>1,
					'view'=>$rs['click'],
					'status'=>1,
					'create_time'=>$rs['pubdate'],
					'picurl'=>$rs['litpic'],
					'content'=>$rs['body']
				];
				$data_pw=['mid'=>1,'uid'=>1];
				Db::name('cms_content1')->insert($data);
				Db::name('cms_content')->insert($data_pw);
			}
if (!empty($list)){
	$page++;
	$pa=$page-1;
	$this->success("内容正在转换中，你可以去喝杯茶歇会... 第 {$pa} 页",url("dedetox1/index/neirong",['page' =>$page]),'',1);
	exit;
}else{
	$this->success('数据转换完成',"index/index/index");
}
		
	}
}
