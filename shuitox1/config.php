<?php
/**
 * 水平凡CMS转齐博X1 
 * Created by qibo168.com.
 * User: suifeng QQ:87211061
 */
return [
	 'system_dirname'=>basename(__DIR__),
	'db_config1'=>[
		'type'=>'mysql',
		'hostname'=>'127.0.0.1',// 水平凡服务器地址
		'database'=>'aysb',// 数据库名
		'username'=>'root',// 数据库用户名
		'password'=>'root',// 数据库密码
		'charset'=>'utf8',// 数据库编码 直接写死 utf8 即可
		'prefix'=>'shuipf_',// 数据库表前缀
	],
];