<?php
/**
 * 水平凡CMS转齐博X1 
 * Created by qibo168.com.
 * User: suifeng QQ:87211061
 */
namespace app\shuitox1\index;
use app\common\controller\IndexBase;
use think\Db;
class Index extends IndexBase{
  
	/**
	 * 转换首页 开始清理表
	 */
	public function index(){
		$prefix = config('database.prefix');
		Db::execute("TRUNCATE ".$prefix."cms_category");
		Db::execute("TRUNCATE ".$prefix."cms_content");
		Db::execute("TRUNCATE ".$prefix."cms_content1");
		Db::execute("TRUNCATE ".$prefix."cms_content2");
		Db::execute("TRUNCATE ".$prefix."cms_content3");
		Db::execute("TRUNCATE ".$prefix."cms_info");
		Db::execute("TRUNCATE ".$prefix."cms_sort");
	 	$this->success('数据表清空完毕 开始转换栏目数据',"shuitox1/index/lanmu");
	}
 
	/**
	 * 转换栏目  单页类型不转换 需要先在CMS模块创建对应的模型 ID要对应上
	 * @throws \think\Exception
	 */
	public function lanmu(){
		Db::connect('db_config1')->name('category')->where('type',0)->chunk(50,function($user){
			foreach($user as $rs){
				$data=[
					'id'=>$rs['catid'],
					'pid'=>$rs['parentid'],
					//'mid'=>$rs['modelid'], 齐博cms后台创建的模型id和水平凡后台的ID对应了
					'mid'=>1, //不管对应了 转换栏目后手工重新指定模型 如果id对应删除这段打开上面的
					'name'=>$rs['catname']
				];
				Db::name('cms_sort')->insert($data);
			}
		});
		$this->success('栏目转换完毕 开始转换内容',"shuitox1/index/neirong");
	}

	/**
	 * 转换内容 因为水平凡cms的特性 有多少模型需要转换多少次 而且内容必须从1开始自动自增 下面以NEWS 为例
	 * 这里把文章的 id 从1开始重新排列了
	 * @throws \think\Exception
	 */
	public function neirong($page=1){
		$list=Db::connect('db_config1')->name('news')->alias('A')->join('news_data B','A.id = B.id','LEFT')->field('A.*,B.content')->limit(100)->page($page)->select();
		 foreach($list as $rs){
				$rs['content'] = str_replace('/d/file/',"/public/uploads/file/",$rs['content']);
			 $rs['thumb']=str_replace('/d/file/',"/public/uploads/file/",$rs['thumb']);
			$ispic=$rs['thumb']?1:0;
				$data=[
					'fid'=>$rs['catid'],
					'mid'=>1, // 模型ID 要和水平凡的对应上 比如我的news模型对应齐博cms后台模型1
					'title'=>$rs['title'],
					'ispic'=>$ispic,
					'uid'=>1, //谁发布的 默认为1
					'view'=>$rs['views'],
					'status'=>1,
					'create_time'=>$rs['inputtime'],
					'picurl'=>$rs['thumb'],
					'content'=>$rs['content']
				];
				$data_pw=['mid'=>1,'uid'=>1]; // 模型和会员
				Db::name('cms_content1')->insert($data);
				Db::name('cms_content')->insert($data_pw);
			}
if (!empty($list)){
	$page++;
	$pa=$page-1;
	$this->success("内容正在转换中，你可以去喝杯茶歇会... 第 {$pa} 页",url("shuitox1/index/neirong",['page' =>$page]),'',1);
	exit;
}else{
	$this->success('数据转换完成',"index/index/index");
}
		
	}
}
