<?php
/**
 * 齐博门户新闻模块到X1文章系统转换程序
 * Created by qibo168.com.
 * User: suifeng QQ:87211061
 * Date: 2018/3/30
 */
return [
	 'system_dirname'=>basename(__DIR__),
	'db_config1'=>[
		'type'=>'mysql',
		'hostname'=>'127.0.0.1',// 门户程序服务器地址
		'database'=>'qibo80',// 门户程序数据库名
		'username'=>'root',// 门户程序数据库用户名
		'password'=>'root',// 门户程序数据库密码
		'charset'=>'utf8',// 数据库编码 直接写死 utf8 即可
		'prefix'=>'qb_',// 数据库表前缀
	],
];