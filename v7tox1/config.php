<?php
/**
 * 齐博V7 8 9到X1文章系统转换程序
 * Created by qibo168.com.
 * User: suifeng QQ:87211061
 * Date: 2018/3/30
 */
return [
	 'system_dirname'=>basename(__DIR__),
	'db_config1'=>[
		'type'=>'mysql',
		'hostname'=>'127.0.0.1',// V7 8 9服务器地址
		'database'=>'tiedaobing',// V7 8 9数据库名
		'username'=>'root',// V7 8 9数据库用户名
		'password'=>'root',// V7 8 9数据库密码
		'charset'=>'utf8',// 数据库编码 直接写死 utf8 即可
		'prefix'=>'qb_',// 数据库表前缀
	],
];