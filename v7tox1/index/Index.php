<?php
/**
 * 齐博V7 8 9到X1文章系统转换程序
 * Created by qibo168.com.
 * User: suifeng QQ:87211061
 * Date: 2018/3/30
 */

namespace app\v7tox1\index;
use app\common\controller\IndexBase;
use think\Db;
class Index extends IndexBase{
  
	/**
	 * 转换首页 开始清理表
	 */
	public function index(){
		$prefix = config('database.prefix');
		Db::execute("TRUNCATE ".$prefix."cms_category");
		Db::execute("TRUNCATE ".$prefix."cms_content");
		Db::execute("TRUNCATE ".$prefix."cms_content1");
		Db::execute("TRUNCATE ".$prefix."cms_content2");
		Db::execute("TRUNCATE ".$prefix."cms_content3");
		Db::execute("TRUNCATE ".$prefix."cms_info");
		Db::execute("TRUNCATE ".$prefix."cms_sort");
		Db::execute("TRUNCATE ".$prefix."memberdata");
		Db::execute("TRUNCATE ".$prefix."group");
		$this->success('数据表清空完毕 开始转换会员数据',"v7tox1/index/user");
	}

	/**
	 * 转换会员  需要更多字段的自己加
	 * @throws \think\Exception
	 */
	public function user($page=1){
	 $list=Db::connect('db_config1')->name('memberdata')->alias('A')->join('members B','A.uid = B.uid','LEFT')->field('A.*,B.password')->limit(100)->page($page)->select();
		foreach($list as $rs){
			$password_rand= rands(rand(5,10));
		$password = md5($rs['password'].$password_rand);
				$data=[
					'uid'=>$rs['uid'],
					'username'=>$rs['username'],
					'groupid'=>$rs['groupid'],
					'yz'=>$rs['yz'],
					'icon'=>$rs['icon'],
					'password_rand'=>$password_rand,
					'password'=>$password
				];
				 
				Db::name('memberdata')->insert($data);
				 
			}
if (!empty($list)){
	$page++;
	$this->success("会员正在转换中,请稍后...",url("v7tox1/index/user",['page' =>$page]),'',1);
	exit;
}else{
	$this->success('会员转换完毕 开始转换会员组',"v7tox1/index/zu");
}
	}

	/**
	 * 转换会员组
	 * 这里采用了 数据集分批处理
	 * @throws \think\Exception
	 */
	public function zu(){
		Db::connect('db_config1')->name('group')->chunk(50,function($user){
			foreach($user as $rs){
				 if($rs['gid']==3){
					$rs['allowadmindb']='{"base-admin-setting\/index":"1","base-admin-setting\/clearcache":"1","base-admin-plugin\/index":"1","base-admin-plugin\/add":"1","base-admin-plugin\/market":"1","base-admin-plugin\/edit":"1","base-admin-plugin\/delete":"1","base-admin-plugin\/copy":"1","base-admin-module\/index":"1","base-admin-module\/add":"1","base-admin-module\/market":"1","base-admin-module\/edit":"1","base-admin-module\/delete":"1","base-admin-module\/copy":"1","base-admin-hook\/index":"1","base-admin-hook\/add":"1","base-admin-hook\/edit":"1","base-admin-hook\/delete":"1","base-admin-hook_plugin\/market":"1","base-admin-hook_plugin\/index":"1","base-admin-hook_plugin\/add":"1","base-admin-hook_plugin\/edit":"1","base-admin-hook_plugin\/delete":"1","base-admin-admin_menu\/index":"1","base-admin-admin_menu\/add":"1","base-admin-admin_menu\/edit":"1","base-admin-admin_menu\/delete":"1","base-admin-webmenu\/index":"1","base-admin-webmenu\/add":"1","base-admin-webmenu\/edit":"1","base-admin-webmenu\/delete":"1","base-admin-alonepage\/index":"1","base-admin-alonepage\/add":"1","base-admin-alonepage\/edit":"1","base-admin-alonepage\/delete":"1","base-admin-style\/market":"1","base-admin-style\/add":"1","base-admin-upgrade\/index":"1","base-admin-upgrade\/sysup":"1","base-admin-upgrade\/check_files":"1","base-admin-upgrade\/view_file":"1","base-admin-mysql\/index":"1","base-admin-mysql\/backup":"1","base-admin-mysql\/showtable":"1","base-admin-mysql\/into":"1","base-admin-mysql\/tool":"1","member-admin-member\/index":"1","member-admin-member\/add":"1","member-admin-member\/edit":"1","member-admin-member\/delete":"1","member-admin-group\/index":"1","member-admin-group\/add":"1","member-admin-group\/edit":"1","member-admin-group\/delete":"1","member-admin-group\/admin_power":"1","module-cms-setting\/index":"1","module-cms-content\/postnew":"1","module-cms-content\/index":"1","module-cms-content\/add":"1","module-cms-content\/edit":"1","module-cms-content\/delete":"1","module-cms-sort\/index":"1","module-cms-sort\/add":"1","module-cms-sort\/edit":"1","module-cms-sort\/delete":"1","module-cms-module\/index":"1","module-cms-module\/add":"1","module-cms-module\/edit":"1","module-cms-module\/delete":"1","module-cms-field\/index":"1","module-cms-field\/add":"1","module-cms-field\/edit":"1","module-cms-field\/delete":"1","module-cms-category\/index":"1","module-cms-category\/add":"1","module-cms-category\/edit":"1","module-cms-category\/delete":"1","module-cms-info\/index":"1","module-cms-info\/add":"1","module-cms-info\/edit":"1","module-cms-info\/delete":"1","module-shop-setting\/index":"1","module-shop-content\/postnew":"1","module-shop-content\/index":"1","module-shop-content\/add":"1","module-shop-content\/edit":"1","module-shop-content\/delete":"1","module-shop-sort\/index":"1","module-shop-sort\/add":"1","module-shop-sort\/edit":"1","module-shop-sort\/delete":"1","module-shop-module\/index":"1","module-shop-module\/add":"1","module-shop-module\/edit":"1","module-shop-module\/delete":"1","module-shop-field\/index":"1","module-shop-field\/add":"1","module-shop-field\/edit":"1","module-shop-field\/delete":"1","module-shop-order\/index":"1","module-shop-order\/edit":"1","module-shop-order\/delete":"1","plugin-log-action\/index":"1","plugin-log-action\/delete":"1","plugin-log-login\/index":"1","plugin-log-login\/delete":"1","plugin-weixin-setting\/index":"1","plugin-weixin-menu\/config":"1","plugin-weixin-weixin_autoreply\/index":"1","plugin-weixin-weixin_autoreply\/add":"1","plugin-weixin-weixin_autoreply\/edit":"1","plugin-weixin-weixin_autoreply\/delete":"1","plugin-weixin-weixin_msg\/index":"1","plugin-config_set-config\/index":"1","plugin-config_set-config\/add":"1","plugin-config_set-config\/edit":"1","plugin-config_set-config\/delete":"1","plugin-config_set-group\/index":"1","plugin-config_set-group\/add":"1","plugin-config_set-group\/edit":"1","plugin-config_set-group\/delete":"1","plugin-smsali-setting\/index":"1","plugin-label-applabel\/index":"1","plugin-label-applabel\/add":"1","plugin-label-applabel\/edit":"1","plugin-label-applabel\/delete":"1","plugin-label-applabel\/set":"1","plugin-login-setting\/index":"1","plugin-comment-content\/index":"1","plugin-comment-content\/delete":"1","plugin-area-province\/index":"1","plugin-area-province\/add":"1","plugin-area-province\/edit":"1","plugin-area-province\/delete":"1","plugin-area-city\/index":"1","plugin-area-city\/add":"1","plugin-area-city\/edit":"1","plugin-area-city\/delete":"1","plugin-area-zone\/index":"1","plugin-area-zone\/add":"1","plugin-area-zone\/edit":"1","plugin-area-zone\/delete":"1","plugin-area-street\/index":"1","plugin-area-street\/add":"1","plugin-area-street\/edit":"1","plugin-area-street\/delete":"1","plugin-marketing-rmb_getout\/index":"1","plugin-marketing-rmb_getout\/delete":"1","plugin-marketing-rmb_infull\/index":"1","plugin-marketing-rmb_infull\/delete":"1","plugin-marketing-rmb_consume\/index":"1","plugin-marketing-rmb_consume\/delete":"1","plugin-marketing-moneylog\/index":"1","plugin-marketing-moneylog\/delete":"1"}'; 
				 }
				$data=[
					'id'=>$rs['gid'],
					'type'=>$rs['gptype'],
					'title'=>$rs['grouptitle'],
					'level'=>$rs['levelnum'],
					'powerdb'=>$rs['powerdb'],
					'allowadmin'=>$rs['allowadmin'],
					'admindb'=>$rs['allowadmindb'],
				];
				Db::name('group')->insert($data);
				 
			}
		});
		$this->success('会员组转换完毕 开始转换栏目',"v7tox1/index/lanmu");
	}
	/**
	 * 转换栏目 每次处理50个
	 * 栏目仅仅是转换了文章类型的 单页 和其他模块的暂时不处理
	 * @throws \think\Exception
	 */
	public function lanmu(){
		Db::connect('db_config1')->name('sort')->where('fmid','eq',0)->where('type','NEQ',2)->chunk(50,function($user){
			foreach($user as $rs){
				$data=[
					'id'=>$rs['fid'],
					'pid'=>$rs['fup'],
					'mid'=>1,
					'name'=>$rs['name']
				];
				Db::name('cms_sort')->insert($data);
			}
		});
		$this->success('栏目转换完毕 开始转换内容',"v7tox1/index/neirong");
	}

	/**
	 * 转换内容 每次100个 只转换文章模型的 其他模型的暂时不处理
	 * 这里把文章的 id 从0开始重新排列了
	 * @throws \think\Exception
	 */
	public function neirong($page=1){
		$list=Db::connect('db_config1')->name('article')->alias('A')->join('reply B','A.aid = B.aid','LEFT')->field('A.*,B.content')->where('A.mid','eq',0)->limit(100)->page($page)->select();
		 foreach($list as $rs){
				$rs['content'] = str_replace('http://www_qibosoft_com/Tmp_updir/',"/public/uploads/",$rs['content']);
				$data=[
					'id'=>$rs['id'],
					'fid'=>$rs['fid'],
					'mid'=>1,
					'title'=>$rs['title'],
					'ispic'=>$rs['ispic'],
					'uid'=>$rs['uid'],
					'view'=>$rs['hits'],
					'status'=>$rs['yz'],
					'create_time'=>$rs['posttime'],
					'picurl'=>$rs['picurl'],
					'content'=>$rs['content']
				];
				$data_pw=['mid'=>1,'uid'=>$rs['uid'],'id'=>$rs['id'],'title'=>$rs['title']];
				Db::name('cms_content1')->insert($data);
				Db::name('cms_content')->insert($data_pw);
			}
if (!empty($list)){
	$page++;
	$pa=$page-1;
	$this->success("内容正在转换中，你可以去喝杯茶歇会... 第 {$pa} 页",url("v7tox1/index/neirong",['page' =>$page]),'',1);
	exit;
}else{
	$this->success('数据转换完成',"index/index/index");
}
		
	}
}
